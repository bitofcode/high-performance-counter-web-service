package com.bitofcode.beint.hpc.highperformancecounter.counter.control;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This represent a counter which counts the appearance of a given combination only once.
 */
public class UniqueCombinationCounter<K1 extends Comparable<? super K1>, K2> {
    private final Map<K1, Map<K2, K1>> data = new ConcurrentHashMap<>();

    /**
     * Add a combination to be counted
     */
    public void add(K1 keyOne, K2 keyTwo) {
        data.computeIfAbsent(keyOne, l -> new ConcurrentHashMap<>(1000)).put(keyTwo, keyOne);
    }

    /**
     * retrieves the current number of elements for the given keyOne.
     */
    public int getCountFor(K1 keyOne) {
        var stringLongMap = data.get(keyOne);
        return (stringLongMap != null) ? stringLongMap.size() : 0;
    }

    public void resetCounterFor(K1 keyOne) {
        data.remove(keyOne);
    }

    public List<K1> allCountersName() {
        var keySet = data.keySet();
        var list = new ArrayList<K1>(keySet.size());
        list.addAll(keySet);
        Collections.sort(list);
        return list;
    }

    @PreDestroy
    public void reset() {
        data.clear();
    }
}
