package com.bitofcode.beint.hpc.highperformancecounter.counter.boundary;

import com.bitofcode.beint.hpc.highperformancecounter.counter.control.TimeProvider;
import com.bitofcode.beint.hpc.highperformancecounter.counter.control.UniqueCombinationCounter;
import com.bitofcode.beint.hpc.highperformancecounter.counter.control.WebClientWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.function.Consumer;

@RestController
@RequestMapping(path = "/api/smaato/accept")
public class CounterWebEndpoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(CounterWebEndpoint.class);
    public static final String FAILED = "failed";
    public static final String OK = "ok";

    private final WebClientWrapper webClientWrapper;
    private final TimeProvider timeProvider;
    private final UniqueCombinationCounter<Long, Integer> counter;
    private final boolean isPostAllowed;
    private final boolean isGetAllowed;

    public CounterWebEndpoint(
            WebClientWrapper webClientWrapper,
            UniqueCombinationCounter<Long, Integer> counter,
            TimeProvider timeProvider,
            @Value("${counter.allowPost:false}") boolean isPostAllowed,
            @Value("${counter.allowGet:true}") boolean isGetAllowed) {
        this.webClientWrapper = webClientWrapper;
        this.counter = counter;
        this.timeProvider = timeProvider;
        this.isPostAllowed = isPostAllowed;
        this.isGetAllowed = isGetAllowed;
    }

    @GetMapping
    public Mono<String> get(
            @RequestParam(name = "id", required = false, defaultValue = "") String id,
            @RequestParam(name = "endpoint", required = false, defaultValue = "") String endpoint
    ) {
        if (id == null || id.isBlank()) {
            return failed();
        }

        incrementCounterFor(id.trim());

        if (shouldHandleEndpoint(endpoint)) {
            handleEndpoint(endpoint);
        }
        return Mono.just(OK);
    }

    private boolean shouldHandleEndpoint(String endpoint) {
        return endpoint != null && !endpoint.trim().equals("") && (isGetAllowed || isPostAllowed);
    }

    private void handleEndpoint(String endpoint) {
        fireHttpRequest(endpoint);
    }

    private void fireHttpRequest(String endpoint) {
        if (isGetAllowed) {
            executeHttpGet(endpoint);
        }
        if (isPostAllowed) {
            executeHttpPost(endpoint, "{\"count\":" + counter.getCountFor(timeProvider.currentMinute()) + "}");
        }
    }

    private void executeHttpPost(String url, String jsonPayload) {
        try {
            var mono = webClientWrapper.post(new URI(url), jsonPayload, MediaType.APPLICATION_JSON);
            mono.subscribe(logResponseOf(url));
        } catch (URISyntaxException e) {
            LOGGER.warn("URI-Syntax of '{}' is not valid", url, e);
            throw new IllegalArgumentException(url);
        }
    }

    private Consumer<ResponseEntity<String>> logResponseOf(String url) {
        return responseEntity -> LOGGER.info("Request to {} returned with => {}", url, responseEntity.getStatusCode());
    }

    private void incrementCounterFor(String id) {
        counter.add(timeProvider.currentMinute(), id.hashCode());
    }

    private Mono<String> failed() {
        return Mono.just(FAILED);
    }

    private void executeHttpGet(String url) {
        try {
            var mono = webClientWrapper.get(new URI(url));
            mono.subscribe(logResponseOf(url));
        } catch (URISyntaxException e) {
            LOGGER.warn("URI-Syntax of '{}' is not valid", url, e);
            throw new IllegalArgumentException(url);
        }
    }
}
