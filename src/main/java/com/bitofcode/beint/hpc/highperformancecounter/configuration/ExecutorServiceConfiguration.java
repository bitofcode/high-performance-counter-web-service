package com.bitofcode.beint.hpc.highperformancecounter.configuration;

import org.springframework.boot.autoconfigure.task.TaskExecutionProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.task.TaskExecutorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ExecutorServiceConfiguration {

    @Bean
    @Primary
    public TaskExecutionProperties taskExecutionProperties() {
        return new TaskExecutionProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "counter.local-threads")
    public TaskExecutionProperties local() {
        return new TaskExecutionProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "counter.remote-threads")
    public TaskExecutionProperties remote() {
        return new TaskExecutionProperties();
    }

    @Bean(destroyMethod = "destroy")
    public ThreadPoolTaskExecutor localExecutorService(TaskExecutionProperties local) {
        return buildExecutor(local);
    }

    private ThreadPoolTaskExecutor buildExecutor(TaskExecutionProperties properties) {
        ThreadPoolTaskExecutor taskExecutor = new TaskExecutorBuilder()
                .corePoolSize(properties.getPool().getCoreSize())
                .maxPoolSize(properties.getPool().getMaxSize())
                .queueCapacity(properties.getPool().getQueueCapacity())
                .awaitTermination(properties.getShutdown().isAwaitTermination())
                .awaitTerminationPeriod(properties.getShutdown().getAwaitTerminationPeriod())
                .threadNamePrefix(properties.getThreadNamePrefix())
                .build();
        taskExecutor.initialize();
        return taskExecutor;
    }

    @Bean(destroyMethod = "destroy")
    public ThreadPoolTaskExecutor remoteExecutorService(TaskExecutionProperties remote) {
        return buildExecutor(remote);
    }
}
