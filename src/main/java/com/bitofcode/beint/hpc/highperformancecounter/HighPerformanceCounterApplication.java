package com.bitofcode.beint.hpc.highperformancecounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
@EnableScheduling
@EnableWebFlux
public class HighPerformanceCounterApplication {

    public static void main(String[] args) {
        SpringApplication.run(HighPerformanceCounterApplication.class, args);
    }

}
