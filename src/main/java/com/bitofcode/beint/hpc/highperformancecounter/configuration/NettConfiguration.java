package com.bitofcode.beint.hpc.highperformancecounter.configuration;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import reactor.netty.http.client.HttpClient;

@Configuration
public class NettConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "counter.http-client")
    public SocketProperties httpClientSocketProperties() {
        return new SocketProperties();
    }

    @Bean
    public WebClientCustomizer webClientCustomizer(HttpClient httpClient) {
        return webClientBuilder -> webClientBuilder.clientConnector(new ReactorClientHttpConnector(httpClient));
    }

    @Bean
    public HttpClient httpClient(SocketProperties httpClientSocketProperties) {
        return HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, httpClientSocketProperties.getConnectionTimeoutMillis())
                .doOnConnected(conn -> conn
                        .addHandlerLast(new ReadTimeoutHandler(httpClientSocketProperties.getReadTimeoutSeconds()))
                        .addHandlerLast(new WriteTimeoutHandler(httpClientSocketProperties.getWriteTimeoutSeconds())));
    }
}
