package com.bitofcode.beint.hpc.highperformancecounter.counter.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.net.URI;

@Component
public class WebClientWrapper {
    private final WebClient.Builder webClientBuilder;
    private final Scheduler scheduler;

    @Autowired
    public WebClientWrapper(WebClient.Builder webClientBuilder, ThreadPoolTaskExecutor remoteExecutorService) {
        this.webClientBuilder = webClientBuilder;
        this.scheduler = Schedulers.fromExecutor(remoteExecutorService);
    }

    /**
     * Visible for unit-test only
     */
    WebClientWrapper(WebClient.Builder webClientBuilder, Scheduler scheduler) {
        this.webClientBuilder = webClientBuilder;
        this.scheduler = scheduler;
    }

    public Mono<ResponseEntity<String>> get(URI uri) {
        return getWebClient(uri)
                .get()
                .retrieve()
                .toEntity(String.class)
                .publishOn(scheduler);
    }

    private WebClient getWebClient(URI uri) {
        return webClientBuilder
                .baseUrl(uri.toString())
                // .filters(functionList -> functionList.addAll(this.exchangeFilterFunctions))
                .build();
    }

    public Mono<ResponseEntity<String>> post(URI uri, String data, MediaType contentType) {
        return getWebClient(uri).post()
                .contentType(contentType)
                .bodyValue(data)
                .retrieve()
                .toEntity(String.class)
                .publishOn(scheduler);
    }
}
