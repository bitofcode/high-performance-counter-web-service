package com.bitofcode.beint.hpc.highperformancecounter.configuration;

import com.bitofcode.beint.hpc.highperformancecounter.counter.control.UniqueCombinationCounter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CounterConfiguration {

    @Bean
    public UniqueCombinationCounter<Long, Integer> counter() {
        return new UniqueCombinationCounter<>();
    }
}
