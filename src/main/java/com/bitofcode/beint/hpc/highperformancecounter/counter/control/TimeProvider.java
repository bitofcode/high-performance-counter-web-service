package com.bitofcode.beint.hpc.highperformancecounter.counter.control;

import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;

@Component
public class TimeProvider {

    public long currentMinute() {
        return Clock.systemUTC().millis() / 60_000;
    }

    public Instant fromMinute(long minute) {
        return Instant.ofEpochSecond(minute * 60);
    }
}
