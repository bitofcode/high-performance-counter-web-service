package com.bitofcode.beint.hpc.highperformancecounter.configuration;

public class SocketProperties {
    private int connectionTimeoutMillis;
    private int readTimeoutSeconds;
    private int writeTimeoutSeconds;

    public int getConnectionTimeoutMillis() {
        return connectionTimeoutMillis;
    }

    public void setConnectionTimeoutMillis(int connectionTimeoutMillis) {
        this.connectionTimeoutMillis = connectionTimeoutMillis;
    }

    public int getReadTimeoutSeconds() {
        return readTimeoutSeconds;
    }

    public void setReadTimeoutSeconds(int readTimeoutSeconds) {
        this.readTimeoutSeconds = readTimeoutSeconds;
    }

    public int getWriteTimeoutSeconds() {
        return writeTimeoutSeconds;
    }

    public void setWriteTimeoutSeconds(int writeTimeoutSeconds) {
        this.writeTimeoutSeconds = writeTimeoutSeconds;
    }

    @Override
    public String toString() {
        return "SocketProperties{" +
                "connectionTimeoutMillis=" + connectionTimeoutMillis +
                ", readTimeoutSeconds=" + readTimeoutSeconds +
                ", writeTimeoutSeconds=" + writeTimeoutSeconds +
                '}';
    }

}
