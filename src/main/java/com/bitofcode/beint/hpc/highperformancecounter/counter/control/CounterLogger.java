package com.bitofcode.beint.hpc.highperformancecounter.counter.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CounterLogger {
    private static final Logger LOGGER = LoggerFactory.getLogger(CounterLogger.class);

    private final UniqueCombinationCounter<Long, ?> counter;
    private final TimeProvider timeProvider;

    public CounterLogger(UniqueCombinationCounter<Long, ?> counter, TimeProvider timeProvider) {
        this.counter = counter;
        this.timeProvider = timeProvider;
    }

    @Scheduled(fixedRate = 60000, initialDelay = 0)
    public void printAndReset() {
        var currentMinute = timeProvider.currentMinute();
        LOGGER.info("Print requests for all Minute before {}", timeProvider.fromMinute(currentMinute));
        counter.allCountersName()
                .stream()
                .filter(minute -> minute < currentMinute)
                .forEach(this::printAndRemove);
    }

    private void printAndRemove(Long minute) {
        LOGGER.info("Minute {} \t Requests {}", timeProvider.fromMinute(minute), counter.getCountFor(minute));
        counter.resetCounterFor(minute);
    }
}
