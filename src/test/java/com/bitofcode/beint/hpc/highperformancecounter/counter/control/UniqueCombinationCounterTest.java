package com.bitofcode.beint.hpc.highperformancecounter.counter.control;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniqueCombinationCounterTest {

    private UniqueCombinationCounter<Long, String> cut;

    @BeforeEach
    void setUp() {
        cut = new UniqueCombinationCounter<>();
    }

    @Test
    void addWithEqualIdsSameMinuteTest() {
        cut.add(1L, "hello");
        cut.add(1L, "hello");

        assertEquals(1, cut.getCountFor(1L));
    }

    @Test
    void addWithDifferentIdsSameMinuteTest() {
        cut.add(1L, "hello");
        cut.add(1L, "hey");

        assertEquals(2, cut.getCountFor(1L));
    }

    @Test
    void addWithDifferentMinuteTest() {
        cut.add(1L, "hello");
        cut.add(2L, "hey");

        assertEquals(1, cut.getCountFor(1L));
        assertEquals(1, cut.getCountFor(2L));
    }

    @Test
    void nonExistingCounterIsAlwaysZeroTest() {
        cut.add(1L, "hello");

        assertEquals(1, cut.getCountFor(1L));
        assertEquals(0, cut.getCountFor(2L));
    }

    @Test
    void emptyAtTheBeginningTest() {
        assertTrue(cut.allCountersName().isEmpty());
    }

    @Test
    void resetOneCounterOnlyTest() {
        cut.add(1L, "hello");
        cut.add(2L, "hello");

        cut.resetCounterFor(1L);

        assertEquals(0, cut.getCountFor(1L));
        assertEquals(1, cut.getCountFor(2L));
    }

    @Test
    void resetAllCountersTest() {
        cut.add(1L, "hello");
        cut.add(2L, "hello");

        cut.reset();

        assertEquals(0, cut.getCountFor(1L));
        assertEquals(0, cut.getCountFor(2L));
        assertTrue(cut.allCountersName().isEmpty());
    }
}