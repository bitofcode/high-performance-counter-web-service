package com.bitofcode.beint.hpc.highperformancecounter.counter.control;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.scheduler.Scheduler;

import java.net.URI;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class WebClientWrapperTest {

    private WebClientWrapper cut;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private WebClient.Builder webClientBuilder;
    @Mock
    private Scheduler scheduler;

    @BeforeEach
    void setUp() {
        cut = new WebClientWrapper(webClientBuilder, scheduler);
    }

    @Test
    void getTest() throws Exception {
        var url = "https://google.com";
        cut.get(new URI(url));

        verify(webClientBuilder).baseUrl(url);
        verify(webClientBuilder.baseUrl(url).build()).get();

        // This is intentionally left in source code and not removed to show you I would maybe test this method.
        // I could use Mockito Extension to verify and mock final/private/equals()/hashCode() methods
        // and i decided to not do that to minimize hard coupling to the testing framework (mockito).
        // verify(webClientBuilder.baseUrl(url).build().get().retrieve().toEntity(String.class)).publishOn(scheduler);
    }

    @Test
    void postTest() throws Exception {
        var url = "https://google.com";
        var data = "{}";
        cut.post(new URI(url), data, MediaType.APPLICATION_JSON);

        verify(webClientBuilder).baseUrl(url);
        verify(webClientBuilder.baseUrl(url).build()).post();
        verify(webClientBuilder.baseUrl(url).build().post()).contentType(MediaType.APPLICATION_JSON);
        verify(webClientBuilder.baseUrl(url).build().post().contentType(MediaType.APPLICATION_JSON)).bodyValue(data);

        // This is intentionally left in source code and not removed to show you I would maybe test this method.
        // I could use Mockito Extension to verify and mock final/private/equals()/hashCode() methods
        // and i decided to not do that to minimize hard coupling to the testing framework (mockito).
        // verify(webClientBuilder.baseUrl(url).build().post().contentType(MediaType.APPLICATION_JSON).bodyValue(data).retrieve().toEntity(String.class)).publishOn(scheduler);
    }
}