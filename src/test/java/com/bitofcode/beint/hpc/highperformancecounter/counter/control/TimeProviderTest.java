package com.bitofcode.beint.hpc.highperformancecounter.counter.control;

import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class TimeProviderTest {
    private static final long MINUTE_IN_MILLIS = 60_000;
    public static final int MINUTE_IN_SECONDS = 60;
    private final TimeProvider cut = new TimeProvider();

    @Test
    void currentMinuteTest() {
        long before = Clock.systemUTC().millis() / MINUTE_IN_MILLIS;
        long currentMinute = cut.currentMinute();
        long after = Clock.systemUTC().millis() / MINUTE_IN_MILLIS;

        assertTrue(currentMinute >= before);
        assertTrue(currentMinute <= after);
    }

    @Test
    void name() {
        long minuteOnly = Clock.systemUTC().millis() / MINUTE_IN_MILLIS;

        Instant expected = Instant.ofEpochSecond(minuteOnly * MINUTE_IN_SECONDS);

        assertEquals(expected, cut.fromMinute(minuteOnly));
    }
}