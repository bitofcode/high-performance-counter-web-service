package com.bitofcode.beint.hpc.highperformancecounter.counter.control;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CounterLoggerTest {

    private CounterLogger cut;

    @Mock
    private UniqueCombinationCounter<Long, ?> counter;
    @Mock
    private TimeProvider timeProvider;

    @BeforeEach
    void setUp() {
        cut = new CounterLogger(counter, timeProvider);
    }

    @Test
    void printAndResetTest() {
        when(timeProvider.currentMinute()).thenReturn(10L);
        when(counter.getCountFor(8L)).thenReturn(4);
        when(counter.allCountersName()).thenReturn(List.of(10L, 8L));

        cut.printAndReset();

        verify(timeProvider).currentMinute();
        verify(timeProvider).fromMinute(8L);
        verify(counter).allCountersName();
        verify(counter).resetCounterFor(8L);
        verify(counter).getCountFor(8L);
    }
}