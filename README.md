# High Performance Counter Web Service

Build a Java application - REST service which is able to process 10K requests per second.

* The service has one GET endpoint - `/api/smaato/accept` which is able to accept an `id` as a mandatory query parameter
  and an optional HTTP `endpoint` query parameter. It should return String `ok` if there were no errors processing the
  request and `failed` in case of any errors.

* When the `endpoint` is provided, the service should fire an HTTP GET request to the provided endpoint and log the HTTP
  status code of the request

* Every minute, the application should write the count of all requests your application received in that minute to a log
  file. If during the span of 1 minute, the same `id` is provided more than once, count it only once.

**Extensions**

* *Extension 1*: Instead of firing an HTTP GET request to the endpoint, fire a POST request with the total count of
  requests in the current minute. The data structure of the content can be freely decided

* *Extension 2*: Make sure the id deduplication works also when the endpoint is behind a Load Balancer and 2 instances
  of your application get the same id simultaneously

* *Extension 3*: Instead of writing the count of unique received ids to a log file, send the count of ids to a
  distributed streaming service of your choice.

## How it is implemented

### Usage

* Build the application: `./mvnw clean package`

* Run the application:
  1. `./mvnw spring-boot:run` or
  2. `java -jar ./target/high-performance-counter-0.0.1-SNAPSHOT.jar`

* access the endpoint:
  * `curl -i -XGET 'http://localhost:8080/api/smaato/accept?id=.&endpoint=http%3A%2F%2Fgoogle.com'`
  * `curl -i -XGET 'http://localhost:8080/api/smaato/accept?id=some-value'`

### Language and Frameworks

* Java 11 as a programming language
* Spring Framework (Spring Boot) in combination with WebFlux
* Netty as a web server (Spring default reactive web server)
* Maven as a build tool
* JUnit 5 for Unit-Tests

### Design Decisions

* **Bulkhead** Decoupling Incoming requests from processing the outgoing requests (more than
  one `ThreadPoolTaskExecutor`)
* **Dependency Injection** Separation between Object-Graph creation and Object-Graph usage (Spring framework)
* **Boundary-Control-Entity** Pattern from  *Ivar Jacobson* for organizing the
  classes (https://en.wikipedia.org/wiki/Entity-control-boundary)
* **Scheduling** to print the count per minute (Spring framework)
* **feature flags** to allow some features to be enabled or disabled
* **configuration** the application can be fine-tuned (e.g. Thread-Pool sizing, etc) by modifing the `application.yml`
* **No integration tests** the time is very limited, and I could not get End-2-End tests done

### Implementation

* The endpoint is implemented as a Spring RestController
  in `com.bitofcode.beint.hpc.highperformancecounter.counter.boundary.CounterWebEndpoint`
  The controller accept request with the HTTP-GET path `/api/smaato/accept`. The handler method checks if an `id` is
  provided and increment the counter for the current minute and id. The handler method checks if an `endpoint` is
  provided and calls the `get` and/or `set` method of `WebClientWrapper` - this is handled in a different ThreadPool
  than the request handling.

* The counter is implemented using two nested `java.util.concurrent.ConcurrentHashMap`
  in `com.bitofcode.beint.hpc.highperformancecounter.counter.control.UniqueCombinationCounter`

* The current minute is provided by an instance
  of `com.bitofcode.beint.hpc.highperformancecounter.counter.control.TimeProvider`

* To write the counter state each minute I have implemented it
  in `com.bitofcode.beint.hpc.highperformancecounter.counter.control.CounterLogger`
  which logs the counter value of each minute.

* To communicate with external web endpoints
  the `com.bitofcode.beint.hpc.highperformancecounter.counter.control.WebClientWrapper` provides convenient methods
  like `get` and `post`

* Spring Java-Configuration can be found in the package `com.bitofcode.beint.hpc.highperformancecounter.configuration`

* All classes in the package `com.bitofcode.beint.hpc.highperformancecounter.counter.control` are unit-tested in
  folder `./src/test/java`

#### Switches for the features

#### Feature "Send HTTP-Get request to the provided Endpoint"

This feature can be enabled and disabled by setting the property `allowGet` to `true`/`false` in the `application.yml`
file.

#### Feature "Send HTTP-Post request to the provided Endpoint with the current counter value"

This feature can be enabled and disabled by setting the property `allowPost` to `true`/`false` in the `application.yml`
file.

### Benchmark

* Apache Benchmark (Version 2.3 <$Revision: 1879490 $>)
* Hardware: **MacBook Pro 2019** with **2,4 GHz 8-Core Intel Core i9** and **64 GB 2667 MHz DDR4**
* OS: macOS Big Sur (version 11.4)

Result Without Endpoint: (Requests per second:    11923.83 [#/sec] (mean))

````shell
ab -n 3000 -c 8  http://localhost:8080/api/smaato/accept\?id\=.
This is ApacheBench, Version 2.3 <$Revision: 1879490 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 300 requests
Completed 600 requests
Completed 900 requests
Completed 1200 requests
Completed 1500 requests
Completed 1800 requests
Completed 2100 requests
Completed 2400 requests
Completed 2700 requests
Completed 3000 requests
Finished 3000 requests


Server Software:        
Server Hostname:        localhost
Server Port:            8080

Document Path:          /api/smaato/accept?id=.
Document Length:        2 bytes

Concurrency Level:      8
Time taken for tests:   0.252 seconds
Complete requests:      3000
Failed requests:        0
Total transferred:      240000 bytes
HTML transferred:       6000 bytes
Requests per second:    11923.83 [#/sec] (mean)
Time per request:       0.671 [ms] (mean)
Time per request:       0.084 [ms] (mean, across all concurrent requests)
Transfer rate:          931.55 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       0
Processing:     0    0   0.1      0       1
Waiting:        0    0   0.1      0       1
Total:          0    1   0.1      1       2

Percentage of the requests served within a certain time (ms)
  50%      1
  66%      1
  75%      1
  80%      1
  90%      1
  95%      1
  98%      1
  99%      1
 100%      2 (longest request)
````

Result With Endpoint: (Requests per second:    3452.82 [#/sec] (mean))
```shell
ab -n 3000 -c 8  http://localhost:8080/api/smaato/accept\?id\=.\&endpoint\=http%3A%2F%2Fgoogle.com
This is ApacheBench, Version 2.3 <$Revision: 1879490 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 300 requests
Completed 600 requests
Completed 900 requests
Completed 1200 requests
Completed 1500 requests
Completed 1800 requests
Completed 2100 requests
Completed 2400 requests
Completed 2700 requests
Completed 3000 requests
Finished 3000 requests


Server Software:        
Server Hostname:        localhost
Server Port:            8080

Document Path:          /api/smaato/accept?id=.&endpoint=http%3A%2F%2Fgoogle.com
Document Length:        2 bytes

Concurrency Level:      8
Time taken for tests:   0.869 seconds
Complete requests:      3000
Failed requests:        0
Total transferred:      240000 bytes
HTML transferred:       6000 bytes
Requests per second:    3452.82 [#/sec] (mean)
Time per request:       2.317 [ms] (mean)
Time per request:       0.290 [ms] (mean, across all concurrent requests)
Transfer rate:          269.75 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       1
Processing:     1    2   1.8      2      32
Waiting:        1    2   1.8      2      32
Total:          1    2   1.8      2      32

Percentage of the requests served within a certain time (ms)
  50%      2
  66%      2
  75%      3
  80%      3
  90%      3
  95%      4
  98%      4
  99%      6
 100%     32 (longest request)
```
